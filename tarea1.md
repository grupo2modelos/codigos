# Análisis de Convergencia distribución Normal

## Parte II

### Codigos grafico binomial

par(mfrow=c(2, 3))
for(p in seq(0, 1, len=6))
{
    x <- dbinom(0:20, size=20, p=p)
    barplot(x, names.arg=0:20, space=1)
}


### Codigos binomial

dbinom(0,size=20,0)
dbinom(4,size=20,0.2)
dbinom(8,size=20,0.4)
dbinom(12,size=20,0.6)
dbinom(16,size=20,0.8)
dbinom(20,size=20,1)


### Codigos de grafico Normal

x <- seq(-5, 20, by = .1)
y <- dnorm(x, mean = 0, sd = 0)
plot(x,y)


x <- seq(-5, 20, by = .1)
y <- dnorm(x, mean = 4, sd = 1.7889)
plot(x,y)


x <- seq(-5, 20, by = .1)
y <- dnorm(x, mean = 8, sd = 2.1909)
plot(x,y)


x <- seq(-5, 20, by = .1)
y <- dnorm(x, mean = 12, sd = 2.1909)
plot(x,y)


x <- seq(-5, 20, by = .1)
y <- dnorm(x, mean = 16, sd = 1.7889)
plot(x,y)


x <- seq(-5, 20, by = .1)
y <- dnorm(x, mean = 20, sd = 0)
plot(x,y)


### Codigos de maximo normal

max(dnorm(x, mean = 0, sd = 0))
max(dnorm(x, mean = 4, sd = 1.7889))
max(dnorm(x, mean = 8, sd = 2.1909))
max(dnorm(x, mean = 12, sd = 2.1909))
max(dnorm(x, mean = 16, sd = 1.7889))
max(dnorm(x, mean = 20, sd = 0))

# Convergencia de la función Binomial 

## Parte II 

### Gráficos relación Binomial y Poisson

x <- seq(0, 20, by = 1)
y <- dbinom(0:20, size = 80, p = 0.05)
plot(x,y)

x <- seq(0, 20, by = 1)
y <- dpois(0:20, lambda = 4)
plot(x,y)


## Parte III

### Gráficos relación Exponencial y Geométrica

x <- seq(0, 80, by = 1)
y <- dexp(0:80, 0.05)
plot(x,y)

x <- seq(0, 80, by = 1)
y <- dgeom(0:80, 0.05)
plot(x,y)